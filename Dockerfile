FROM golang:1.19 as builder

RUN echo "Asia/Shanghai" > /etc/timezone \
    && rm /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

ENV GO111MODULE=on
ENV GOPROXY="https://goproxy.io,direct"

WORKDIR /app
COPY . .

RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux go build -o /app/adm .

FROM alpine:latest as prod

RUN apk --no-cache add ca-certificates

WORKDIR /app/

COPY --from=builder /app .

EXPOSE 9999

CMD ["/app/adm"]
