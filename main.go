package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func main() {
	g := gin.Default()

	g.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, "ok222222")
	})

	g.GET("/docker", func(c *gin.Context) {
		c.JSON(http.StatusOK, fmt.Sprintf("DOCKER TEST222: %s", time.Now().String()))
	})

	g.Run(":9999")
}
